__author__ = 'Jason Parent'


def merge(left, right, array):
    i, j = 0, 0
    while i + j < len(array):
        if i == len(left):
            array[i + j] = right[j]
            j += 1
        elif j == len(right):
            array[i + j] = left[i]
            i += 1
        elif left[i] < right[j]:
            array[i + j] = left[i]
            i += 1
        else:
            array[i + j] = right[j]
            j += 1


def merge_sort(array):
    n = len(array)
    if n < 2:
        return
    mid = n // 2
    left = array[0 : mid]
    right = array[mid : n]
    merge_sort(left)
    merge_sort(right)
    merge(left, right, array)


def partition(array, start, end):
    i = start - 1
    pivot = array[end]
    for j in range(start, end):
        if array[j] <= pivot:
            i += 1
            array[i], array[j] = array[j], array[i]
    array[i + 1], array[end] = array[end], array[i + 1]
    return i + 1


def quick_sort(array, start, end):
    if start < end:
        index = partition(array, start, end)
        quick_sort(array, start, index - 1)
        quick_sort(array, index + 1, end)
