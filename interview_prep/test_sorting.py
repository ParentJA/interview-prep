# Third-party imports.
import pytest

# Local imports.
from . import sorting

__author__ = 'Jason Parent'


class TestMergeSort:
    def test_merge_sort(self):
        array = [5, 4, 3, 2, 1]
        sorting.merge_sort(array)
        assert [1, 2, 3, 4, 5] == array


class TestQuickSort:
    def test_quick_sort(self):
        array = [5, 4, 3, 2, 1]
        sorting.quick_sort(array, 0, len(array) - 1)
        assert [1, 2, 3, 4, 5] == array


class TestFindAnyPairUnsortedArray:
    def find_pair_sum(self, items, value):
        index_by_diff = {}
        for index, item in enumerate(items):
            diff = value - item
            if diff in index_by_diff:
                return index_by_diff[diff], index
            index_by_diff[item] = index
        else:
            return None

    def find_pair_closest(self, items, value):
        items.sort()
        pair = (0, 0)
        i = 0
        j = len(items) - 1
        diff = float('inf')
        while j > i:
            temp_diff = abs(value - (items[i] + items[j]))
            if temp_diff < diff:
                pair = (items[i], items[j])
                diff = temp_diff
            if (items[i] + items[j]) > value:
                j -= 1
            else:
                i +=1
        return pair

    def test_find_pair_sum(self):
        """
        Given an unsorted array of integers, find a pair with given sum in it.
        http://www.techiedelight.com/find-pair-with-given-sum-array/
        """
        assert (0, 2) == self.find_pair_sum([8, 7, 2, 5, 3, 1], 10)

    def test_find_closest(self):
        """
        Given a sorted array and a value, find a pair in array whose sum is closest to the value.
        https://www.geeksforgeeks.org/given-sorted-array-number-x-find-pair-array-whose-sum-closest-x/
        """
        assert (22, 30) == self.find_pair_closest([10, 22, 28, 29, 30, 40], 54)


class TestFindAllPairsUnsortedArray:
    def find_pairs_diff(self, items, diff):
        diffs = set()
        for item in items:
            if (item - diff) in diffs:
                yield item, (item - diff)
            if (item + diff) in diffs:
                yield item, (item + diff)
            diffs.add(item)

    def test_find_pairs_diff(self):
        assert [(3, 1), (4, 2), (5, 3)] == list(self.find_pairs_diff([1, 2, 3, 4, 5], 2))


class TestFindMinMaxArray:
    def find_pair_min_diff(self, items):
        items.sort()
        diff = float('inf')
        for i in range(len(items) - 1):
            temp_diff = items[i + 1] - items[i]
            if temp_diff < diff:
                diff = temp_diff
        return diff

    def test_find_pair_min_diff(self):
        """
        Given an unsorted array, find the minimum difference between any pair in given array.
        https://www.geeksforgeeks.org/find-minimum-difference-pair/
        """
        assert 1 == self.find_pair_min_diff([1, 5, 3, 19, 18, 25])
