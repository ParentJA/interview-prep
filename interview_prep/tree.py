__author__ = 'Jason Parent'


class Node:
    def __init__(self, element):
        self.element = element
        self.left, self.right = None, None


def preorder_recursive(node):
    yield node.element
    if node.left:
        yield from preorder_recursive(node.left)
    if node.right:
        yield from preorder_recursive(node.right)


def preorder_iterative(node):
    stack = [node]
    while stack:
        node = stack.pop()
        yield node.element
        if node.right:
            stack.append(node.right)
        if node.left:
            stack.append(node.left)


def inorder_recursive(node):
    if node.left:
        yield from inorder_recursive(node.left)
    yield node.element
    if node.right:
        yield from inorder_recursive(node.right)


def inorder_iterative(node):
    stack = []
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            yield node.element
            node = node.right


def postorder_recursive(node):
    if node.left:
        yield from postorder_recursive(node.left)
    if node.right:
        yield from postorder_recursive(node.right)
    yield node.element


def postorder_iterative(node):
    out = []
    stack = [node]
    while stack:
        node = stack.pop()
        out.append(node.element)
        if node.left:
            stack.append(node.left)
        if node.right:
            stack.append(node.right)
    while out:
        yield out.pop()


def levelorder_iterative(node):
    queue = [node]
    while queue:
        node = queue.pop(0)
        yield node.element
        if node.left:
            queue.append(node.left)
        if node.right:
            queue.append(node.right)


def bst_insert(node, element):
    if node is None:
        return Node(element)
    if element < node.element:
        node.left = bst_insert(node.left, element)
    else:
        node.right = bst_insert(node.right, element)
    return node


def bst_search(node, element):
    if node is None:
        return None
    if node.element == element:
        return node
    if node.element > element:
        return bst_search(node.left, element)
    else:
        return bst_search(node.right, element)
