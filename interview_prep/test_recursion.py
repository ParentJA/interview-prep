# Third-party imports.
import pytest

# Local imports.
from . import recursion

__author__ = 'Jason Parent'


class TestCombinations:
    def test_combinations(self):
        combinations = [combo for combo in recursion.combinations('PYTHON', '', 5, 4)]
        assert ['YTHO', 'PTHO', 'PYHO', 'PYTO', 'PYTH'] == combinations