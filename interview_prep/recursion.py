def combinations(iterable, out, length, target):
    if target > length:
        return
    if target == 0:
        yield out
    for i in range(length - 1, -1, -1):
        yield from combinations(iterable, iterable[i] + out, i, target - 1)
        while i > 0 and iterable[i] == iterable[i - 1]:
            i -= 1


def permutations(array, i):
    if i == len(array):
        yield array
    else:
        for j in range(i, len(array)):
            array[i], array[j] = array[j], array[i]
            yield from permutations(array, i + 1)
            array[i], array[j] = array[j], array[i]
