# Third-party imports.
import pytest

# Local imports.
from . import trie

__author__ = 'Jason Parent'


class TestTrie:
    def test_has_string(self):
        t = trie.Trie()
        t.insert('apples')
        t.insert('bananas')
        t.insert('grapes')
        assert True == t.search('apples')
        assert False == t.search('apple')
    