# Third-party imports.
import pytest

# Local imports.
from . import tree

__author__ = 'Jason Parent'


class TestBinaryTreeTraversal:
    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None

    def setup_method(self):
        #     D
        #    / \
        #   B   E
        #  / \   \
        # A   C   F
        self.node = self.Node('D')
        self.node.left = self.Node('B')
        self.node.right = self.Node('E')
        self.node.left.left = self.Node('A')
        self.node.left.right = self.Node('C')
        self.node.right.right = self.Node('F')

    def test_preorder_recursive(self):
        elements = list(tree.preorder_recursive(self.node))
        assert ['D', 'B', 'A', 'C', 'E', 'F'] == elements

    def test_preorder_iterative(self):
        elements = list(tree.preorder_iterative(self.node))
        assert ['D', 'B', 'A', 'C', 'E', 'F'] == elements

    def test_inorder_recursive(self):
        elements = list(tree.inorder_recursive(self.node))
        assert ['A', 'B', 'C', 'D', 'E', 'F'] == elements

    def test_inorder_iterative(self):
        elements = list(tree.inorder_iterative(self.node))
        assert ['A', 'B', 'C', 'D', 'E', 'F'] == elements

    def test_postorder_recursive(self):
        elements = list(tree.postorder_recursive(self.node))
        assert ['A', 'C', 'B', 'F', 'E', 'D'] == elements

    def test_postorder_iterative(self):
        elements = list(tree.postorder_iterative(self.node))
        assert ['A', 'C', 'B', 'F', 'E', 'D'] == elements

    def test_levelorder(self):
        elements = list(tree.levelorder_iterative(self.node))
        assert ['D', 'B', 'E', 'A', 'C', 'F'] == elements


class TestLevelOrderDifference:
    """
    Given a binary tree, calculate the difference between sum of all nodes present at odd levels and sum of all nodes
    present at even level.

    http://www.techiedelight.com/difference-between-sum-nodes-odd-even-levels/

    Need to know:
    - Level-order tree traversal
    """

    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None
            self.level = 1

    def _level_order_difference(self, node):
        sum_odd = 0
        sum_even = 0
        queue = [node]
        while queue:
            node = queue.pop(0)
            if node.level % 2 == 0:
                sum_even += node.element
            else:
                sum_odd += node.element
            if node.left:
                node.left.level = node.level + 1
                queue.append(node.left)
            if node.right:
                node.right.level = node.level + 1
                queue.append(node.right)
        return sum_odd - sum_even

    def setup_method(self):
        #     1
        #    / \
        #   2   3
        #  /   / \
        # 4   5   6
        #    / \
        #   7   8
        self.node = self.Node(1)
        self.node.left = self.Node(2)
        self.node.right = self.Node(3)
        self.node.left.left = self.Node(4)
        self.node.right.left = self.Node(5)
        self.node.right.right = self.Node(6)
        self.node.right.left.left = self.Node(7)
        self.node.right.left.right = self.Node(8)

    def test_level_order_difference(self):
        assert -4 == self._level_order_difference(self.node)


class TestBinaryTreeSum:
    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None

    def setup_method(self):
        #     4
        #    / \
        #   2   5
        #  / \   \
        # 1   3   6
        self.node = self.Node(4)
        self.node.left = self.Node(2)
        self.node.right = self.Node(5)
        self.node.left.left = self.Node(1)
        self.node.left.right = self.Node(3)
        self.node.right.right = self.Node(6)

    def preorder_sum_recursive(self, node):
        if not node:
            return 0
        return (
            node.element + 
            self.preorder_sum_recursive(node.left) + 
            self.preorder_sum_recursive(node.right)
        )

    def preorder_sum_iterative(self, node):
        s = 0
        stack = [node]
        while stack:
            node = stack.pop()
            s += node.element
            if node.left:
                stack.append(node.left)
            if node.right:
                stack.append(node.right)
        return s

    def test_preorder_sum_recursive(self):
        s = 0
        assert 21 == self.preorder_sum_recursive(self.node)

    def test_preorder_sum_iterative(self):
        assert 21 == self.preorder_sum_iterative(self.node)


class TestLevelOrderLinkedList:
    """
    Given a binary tree, write an efficient algorithm to link nodes at the same level in the form of a linked list
    like structure.

    http://www.techiedelight.com/link-nodes-each-level-binary-tree/

    Need to know:
    - Level-order tree traversal
    - Doubly linked list
    """

    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None
            self.level = 0
            self.prev, self.next = None, None

    def _level_order_linked_list(self, node):
        prev_node = None
        queue = [node]
        while queue:
            node = queue.pop(0)
            if prev_node and node.level == prev_node.level:
                prev_node.next = node
                node.prev = prev_node
            if node.left:
                node.left.level = node.level + 1
                queue.append(node.left)
            if node.right:
                node.right.level = node.level + 1
                queue.append(node.right)
            prev_node = node

    def setup_method(self):
        #     D
        #    / \
        #   B   E
        #  / \   \
        # A   C   F
        self.node = self.Node('D')
        self.node.left = self.Node('B')
        self.node.right = self.Node('E')
        self.node.left.left = self.Node('A')
        self.node.left.right = self.Node('C')
        self.node.right.right = self.Node('F')

    def test_level_order_linked_list(self):
        # Run
        self._level_order_linked_list(self.node)

        # Root
        assert self.node.prev is None
        assert self.node.next is None

        # Level 1
        assert self.node.left.prev is None
        assert self.node.right == self.node.left.next
        assert self.node.left == self.node.right.prev
        assert self.node.right.next is None

        # Level 2
        assert self.node.left.left.prev is None
        assert self.node.left.right == self.node.left.left.next
        assert self.node.left.left == self.node.left.right.prev
        assert self.node.left.right == self.node.right.right.prev
        assert self.node.right.right == self.node.left.right.next
        assert self.node.right.right.next is None


class TestBinaryTreeBuilder:
    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None

    def _build_tree(
        self,
        *,
        preorder,
        preorder_start,
        preorder_end,
        inorder,
        inorder_start,
        inorder_end
    ):
        if preorder_start > preorder_end or inorder_start > inorder_end:
            return None
        element = preorder[preorder_start]
        node = self.Node(element)
        index = inorder.index(element)
        node.left = self._build_tree(
            preorder=preorder,
            preorder_start=preorder_start + 1,
            preorder_end=preorder_start + index - inorder_start,
            inorder=inorder,
            inorder_start=inorder_start,
            inorder_end=index - 1
        )
        node.right = self._build_tree(
            preorder=preorder,
            preorder_start=preorder_start + index - inorder_start + 1,
            preorder_end=preorder_end,
            inorder=inorder,
            inorder_start=index + 1,
            inorder_end=inorder_end
        )
        return node

    def test_tree_builder(self):
        preorder = ['D', 'B', 'A', 'C', 'E', 'F']
        inorder = ['A', 'B', 'C', 'D', 'E', 'F']
        tree = self._build_tree(
            preorder=preorder,
            preorder_start=0,
            preorder_end=len(preorder) - 1,
            inorder=inorder,
            inorder_start=0,
            inorder_end=len(inorder) - 1
        )

        #     D
        #    / \
        #   B   E
        #  / \   \
        # A   C   F
        assert 'D' == tree.element
        assert 'B' == tree.left.element
        assert 'E' == tree.right.element
        assert 'A' == tree.left.left.element
        assert 'C' == tree.left.right.element
        assert 'F' == tree.right.right.element


class TestBSTInsert:
    def test_bst_insert_empty_tree_creates_root(self):
        root = tree.bst_insert(None, 'D')
        assert root.element == 'D'

    def test_bst_insert_element_less_than_root_adds_to_left(self):
        root = tree.bst_insert(None, 'D')
        tree.bst_insert(root, 'B')
        assert root.left.element == 'B'

    def test_bst_insert_element_greater_than_root_adds_to_right(self):
        root = tree.bst_insert(None, 'D')
        tree.bst_insert(root, 'E')
        assert root.right.element == 'E'

    def test_bst_insert_builds_tree_correctly(self):
        root = tree.bst_insert(None, 'D')
        tree.bst_insert(root, 'B')
        tree.bst_insert(root, 'E')
        tree.bst_insert(root, 'A')
        tree.bst_insert(root, 'C')
        tree.bst_insert(root, 'F')

        #     D
        #    / \
        #   B   E
        #  / \   \
        # A   C   F
        assert 'D' == root.element
        assert 'B' == root.left.element
        assert 'E' == root.right.element
        assert 'A' == root.left.left.element
        assert 'C' == root.left.right.element
        assert 'F' == root.right.right.element

        assert ['A', 'B', 'C', 'D', 'E', 'F'] == list(tree.inorder_recursive(root))


class TestBSTSearch:
    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None

    def setup_method(self):
        #     D
        #    / \
        #   B   E
        #  / \   \
        # A   C   F
        self.node = self.Node('D')
        self.node.left = self.Node('B')
        self.node.right = self.Node('E')
        self.node.left.left = self.Node('A')
        self.node.left.right = self.Node('C')
        self.node.right.right = self.Node('F')

    def test_search_element_in_tree(self):
        node = tree.bst_search(self.node, 'C')
        assert 'C' == node.element

    def test_search_element_not_in_tree(self):
        node = tree.bst_search(self.node, 'G')
        assert node is None


class TestSumGreaterKeys:
    class Node:
        def __init__(self, element):
            self.element = element
            self.left, self.right = None, None

    def setup_method(self):
        #     4
        #    / \
        #   2   5
        #  / \   \
        # 1   3   6
        self.node = self.Node(4)
        self.node.left = self.Node(2)
        self.node.right = self.Node(5)
        self.node.left.left = self.Node(1)
        self.node.left.right = self.Node(3)
        self.node.right.right = self.Node(6)

    def sum_greater_keys(self, node):
        s = 0
        stack = []
        while node or stack:
            if node:
                stack.append(node)
                node = node.right
            else:
                node = stack.pop()
                node.element += s
                s = node.element
                node = node.left

    def test_sum_greater_keys(self):
        self.sum_greater_keys(self.node)
        assert [21, 20, 18, 15, 11, 6] == list(tree.inorder_recursive(self.node))
