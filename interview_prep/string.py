# Standard library imports.
from collections import defaultdict

__author__ = 'Jason Parent'


def find_leaf_nodes(array, start, end):
    if start == end:
        print(array[start])
    else:
        i = start + 1
        while i < end and array[i] < array[start]:
            i += 1
        if array[i] > array[start]:
            if (i - 1) > start:
                find_leaf_nodes(array, start + 1, i - 1)
            find_leaf_nodes(array, i, end)
        else:
            find_leaf_nodes(array, start + 1, end)


def window(string, pattern):
    if len(string) < len(pattern):
        return

    count_by_string_char = defaultdict(int)
    count_by_pattern_char = defaultdict(int)
    for char in pattern:
        count_by_pattern_char[char] += 1

    start = 0
    start_index = -1
    min_len = float('-inf')

    count = 0
    for index, char in enumerate(string):
        count_by_string_char[char] += 1
        
        if (
            count_by_pattern_char[char] != 0 and 
            count_by_string_char[char] <= count_by_pattern_char[char]
        ):
            count += 1

        if count == len(pattern):
            start_char = string[start]
            while (
                count_by_string_char[start_char] > count_by_pattern_char[start_char] or 
                count_by_pattern_char[start_char] == 0
            ):
                if count_by_string_char[start_char] > count_by_pattern_char[start_char]:
                    count_by_string_char[start_char] -= 1

                start += 1
                start_char = string[start]
                len_window = index - start + 1
                if min_len > len_window:
                    min_len = len_window
                    start_index = start

    if start_index == -1:
        return

    return string[start_index : start_index + min_len]
