__author__ = 'Jason Parent'


class Trie:
    def __init__(self):
        self.children = dict()
        self.is_leaf = False

    def insert(self, string):
        this = self
        for char in string:
            if not this.children.get(char):
                this.children[char] = Trie()
            this = this.children.get(char)
        this.is_leaf = True

    def search(self, string):
        this = self
        for char in string:
            this = this.children.get(char)
            if not this:
                return False
        return this.is_leaf
