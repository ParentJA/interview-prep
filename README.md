# Interview Prep

## Algorithms

### Pattern Searching

**Options:**
1. Brute-force, O(p × t) time efficiency
1. Knuth-Morris-Pratt algorithm, O(p + t) time efficiency
1. Trie, O(p) time efficiency, space-inefficient, good for repeated queries on a large document
2. Dynamic Programming

### Substrings

#### Memorize

```python
def non_overlapping_substrings(string, i, substrings):
    if i == len(string):
        yield substrings
    for j in range(len(string) - 1, i - 1, -1):
        substring = string[i : j + 1]
        yield from non_overlapping_substrings(string, j + 1, substrings = [substring])
```

### Permutations and Combinations

#### Memorize

```python
def combinations(iterable, out, length, target):
    if target > length:
        return
    if target == 0:
        yield out
    for i in range(length - 1, -1, -1):
        yield from combinations(iterable, iterable[i] + out, i, target - 1)
        while i > 0 and iterable[i] == iterable[i - 1]:
            i -= 1
```

```python
def permutations(array, i):
    if i == len(array):
        yield array
    else:
        for j in range(i, len(array)):
            array[i], array[j] = array[j], array[i]
            yield from permutations(array, i + 1)
            array[i], array[j] = array[j], array[i]
```

### Binary Trees

#### Tips and Tricks

1. Recursive algorithms are the easiest to implement for binary trees.
2. Always think of the base case. It's usually some form of `if not node: ...`.
3. If you need to pass a counter in a recursive function, create a basic object. 

**Example:**

```python
class Counter:
    def __init__(self, value=0):
        self.value = value
```

#### Topics

- Traversals
- Construction and Conversion
- Checking and Searching
- Summation
- Lowest Common Ancestor
- Heaps

#### Memorize

```python
class Node:
    def __init__(self, element):
        self.element = element
        self.left = None
        self.right = None
```

```python
def preorder_recursive(node):
    yield node.element
    if node.left:
        yield from preorder_recursive(node.left)
    if node.right:
        yield from preorder_recursive(node.right)
```

```python
def preorder_iterative(node):
    stack = [node]
    while stack:
        node = stack.pop()
        yield node.element
        if node.right:
            stack.append(node.right)
        if node.left:
            stack.append(node.left)
```

```python
def inorder_recursive(node):
    if node.left:
        yield from inorder_recursive(node.left)
    yield node.element
    if node.right:
        yield from inorder_recursive(node.right)
```

```python
def inorder_iterative(node):
    stack = []
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            yield node.element
            node = node.right
```

```python
def postorder_recursive(node):
    if node.left:
        yield from postorder_recursive(node.left)
    if node.right:
        yield from postorder_recursive(node.right)
    yield node.element
```

```python
def postorder_iterative(node):
    out = []
    stack = [node]
    while stack:
        node = stack.pop()
        out.append(node.element)
        if node.left:
            stack.append(node.left)
        if node.right:
            stack.append(node.right)
    while out:
        yield out.pop()
```

```python
def levelorder_iterative(node):
    queue = [node]
    while queue:
        node = queue.pop(0)
        yield node.element
        if node.left:
            queue.append(node.left)
        if node.right:
            queue.append(node.right)
```

### Graphs

#### Memorize

```python
from collections import defaultdict, namedtuple


class Graph:
    # Option 1: Use namedtuple.
    Edge = namedtuple('Edge', ['source', 'destination'])

    # Option 2: Use class.
    class Edge:
        def __init__(self, source, destination):
            self.source = source
            self.destination = destination

    def __init__(self):
        self.adjacency_list = defaultdict(list)

    def add_edge(self, source, destination):
        self.adjacency_list[source].append(self.Edge(source, destination))
```

```python
def dfs_recursive(graph, vertex, visited):
    visited.add(vertex)
    yield vertex
    incidence_collection = graph.adjacency_list[vertex]
    for edge in incidence_collection:
        destination = edge.destination
        if destination not in visited:
            yield from dfs_recursive(graph, destination, visited)
```

```python
def dfs_iterative(graph, vertex):
    visited = {vertex}
    stack = [vertex]
    while stack:
        vertex = stack.pop()
        yield vertex
        incidence_collection = graph.adjacency_list[vertex]
        for edge in incidence_collection:
            destination = edge.destination
            if destination not in visited:
                visited.add(destination)
                stack.append(destination)
```

```python
def bfs_iterative(graph, vertex):
    visited = {vertex}
    queue = [vertex]
    while queue:
        vertex = queue.pop(0)
        yield vertex
        incidence_collection = graph.adjacency_list[vertex]
        for edge in incidence_collection:
            destination = edge.destination
            if destination not in visited:
                visited.add(destination)
                queue.append(destination)
```

## System Design

### Performance vs. Scalability

- A service is scalable if it results in increased performance in a manner proportional to resources added
- Performance means: serving more units of work, handling larger units of work
- Performance problem when your system is slow for a single user
- Scalability problem when your system is fast for a single user but slow under heavy load

### Latency vs. Throughput

- Latency is time to perform an action or produce a result
- Throughput is number of actions performed or results produced in a period of time
- Aim for maximal throughput with acceptable latency

### Availability vs. Consistency

#### CAP Theorem

- In distributed system, only two of the following are guaranteed:
  + Consistency: every read receives the most recent write or an error
  + Availability: every request receives a response (no guarantee that it is the most recent version of the information)
  + Partition Tolerance: system continues to operate despite arbitrary partioning due to network failures
- Since network is unreliable, you must support partition tolerance
- System must make trade-off between:
  + CP - consistency and partition tolerance
    - waiting for response from partitioned node might result in timeout error
    - choose if business requires atomic reads and writes
  + AP - availability and partition tolerance
    - responses return most recent version of data available on a node, which might not be the latest
    - writes take time to propagate when partition is resolved
    - choose if business needs allow for eventual consistency or when system needs to continue to work despite external errors

#### Consistency Patterns

- With multiple copies of the same data, you must decide how to synchronize them so clients have consistent view of the data
- Weak Consistency
  + After a write, reads may or may not see if
  + Best effort approach is taken
  + Works well for VoIP, video chat, real-time multiplayer games
- Eventual Consistency
  + After a write, reads will eventually see it (typically within milliseconds)
  + Data is replicated asychronously
  + Seen in DNS and email systems
  + Works well in highly available systems
- Strong Consistency
  + After a write, reads will see it
  + Data is replicated sychronously
  + Seen in file systems and relational databases
  + Works well in systems that need transactions

#### Availability Patterns

**Fail-Over:**

- Active-Passive
  + Heartbeats sent between active and passive server on standby
  + Passive server takes over active's IP address and resumes service id heartbeats are interrupted
  + Length of downtime determined by whether passive server is already running in _hot_ standby or has to start up from _cold_ standby
  + Only active server handles traffic
  + Also known as _master-slave failover_
- Active-Active
  + Both servers managing traffice, spreading the load between them
  + DNS needs to know about public IPs of both servers, if public-facing
  + Application logic needs to know about both servers, if internal-facing
  + Also known as _master-master failover_
- Disadvantages
  + Fail-over adds more hardware and complexity
  + Potential for loss of data if the active system fails before newly written data can be replicated to the passive system

### Domain Name System

- Domain Name System (DNS) translates a domain name to an IP address
- DNS is hierarchical, few authoritative servers at the top level
- Router or ISP provide info about which DNS servers to contact when doing lookup
- Lower level DNS servers cache mapping, which could become stale due to DNS propagation delays
- DNS results can also be cached by your browser or OS with a time-to-live (TTL)

> **NS record (name server):** Specifies the DNS servers for your domain/subdomain
> **MX record (mail exchange):** Specifies the mail servers for accepting messages
> **A record (address):** Points a name to an IP address
> **CNAME (canonical):** Points a name to another name or `CNAME` (e.g. `example.com` to `www.example.com`) or to an `A` record

**DNS Services:**

- Managed DNS services like Cloudfare and AWS Route 53
- Can route traffic through various methods:
  + Weighted round robin
  + Latency-based
  + Geolocation-based
- Disadvantages:
  + Accessing DNS server introduces slight delay (mitigated by caching)
  + DNS server management could be complex and is generally managed by governments, ISPs, and large companies
  + DNS services are prone to DDoS attack

### Content Delivery Network

- Globally districuted network of proxy servers
- Serves content from locations closer to each individual user
- Static files (e.g. HTML, CSS, JS), photos, and videos
- AWS CloudFront supports dynamic content
- CDN's DNS resolution will tell clients which server to contact
- Improves performance by:
  + Users receive content at data centers close to them
  + Servers do not have to serve requests that the CDN fulfiils

**Push CDNs:**

- Receive new content whenever changes occur on your server
- You must upload directly to the CDN and rewrite URLs to point to the CDN
- You can configure when content expires and when it is updated
- Content is only updated when it is new or changes, which minimizes traffic but maximizes storage
- Good for sites with small amounts of traffic or content that isn't often updated

**Pull CDNs:**

- Grab new content from your server when use first requests the content
- Leave content on your server and rewrite URLs to point to the CDN
- Slower requests until content is cached on CDN
- TTL determines how long content is cached
- Minimize storage space on the CDN
- Can create redundant traffic if files expire and are pulled before they have changed
- Good for sites with heavy traffic--traffic is spread out more evenly with only recently-requested data on the CDN

**Disadvantages:**

- CDN costs could be sugnificant depending on traffic (but compare to costs of having content served from your own servers)
- Content might be stale if updated before TTL expires
- CDNs require changing URLs for static content to point to the CDN

### Load Balancers

When a user visits a website from their browser, the DNS returns the IP address of the load balancer. The user sends a request to the load balancer via the client. The load balancer chooses an application server to process the request and then returns the response to the client.

Load balancers...
- prevent requests from going to unhealthy servers
- prevent overloading application servers
- help eliminate single points of failure (by distributing requests across multiple servers instead of having one server process all requests)
- can do SSL decryption/encryption
- can implement cookies to do session persistence (route the client to the same application server with every request)

#### Failures

**Active-passive fail-over**

Two load balancers are running, one in active mode and the other in passive mode. The active load balancing server is receiving requests and forwarding them to the application servers. It is also constantly sending the passive load balancer heartbeats. If the passive load balancer stops receiving heartbeats, it takes over the active load balancer's IP address and serves requests from the internet to the application servers itself. The failed load balancer can be restarted in the meantime.

**Active-active fail-over**

Two load balancers are running, both in active mode. Both load balancers are receiving requests and sending them to the application servers. If one load balancer fails, the other can handle all of the incoming requests.

> **Potential Issues**
> - Additional hardware needed.
> - Additional complexity is introduced to the system.
> - Potential for loss of data if the active load balancer fails before the passive load balancer takes over.

#### Types

**Round Robin**

The load balancer maintains a list of server IP addresses. For each incoming request, the load balancer chooses the next server in the list and pairs it with the request. When the load balancer reaches the last server in the list, it circles back to the beginning of the list again.

**Weighted Round Robin**

The load balancer maintains a list of servers. Each server has been performance tested and assigned a weight based on how many requests per second it can handle. Servers with higher throughput are given higher weights. The load balancer chooses servers from the list in a round robin pattern, but sends more requests to servers with higher weights before moving to the next server in the list.

**Least Connection**

The load balancer sends the incoming request to the server that is servicing the least number of active sessions.

**Weighted Least Connection**

If two servers are servicing the same number of active sessions, then the load balancer takes weighting into account. The higher weighted server will receive more requests.

**Source IP Hashing**

The load balancer creates a unique key using the source and destination IP addresses, and directs the client request to a particular server based on that key. It maps the server IP addresses to the unique keys.

**Layer 4**

Load balancer's IP address is advertised to clients (via DNS). Clients record the load balancer's IP address as the destination. (Most browsers cache the IP address to save overhead cost of calling DNS servers with every request.) The load balancer inspects the TCP metadata to make decisions about what server to direct the request to. Most of the time, Layer 4 load balancers are hardware devices running proprietary software.

> In the past, hardware load balancers offered significant performance improvements over software-based load balancers. Those performance boosts are now negligible, given modern hardware on servers.

**Layer 7**

The load balancer inspects the incoming HTTP request and makes decisions about which servers to choose based on that data. The URI, the type of data, or cookies can determine which application servers the load balancer directs traffic to. Modern load balancers operate on Layer 7 and serve as full reverse proxies.

> **Horizontal Scaling**
> The process of adding more commodity hardware to a system to sustain more load. Load balancers help by distributing requests among these scaled-out application servers. Each application server must include the same application code. Servers should be stateless--no user-related data (including sessions). Sessions should be stored in a centralized data store like a database or cache. These downstream data store servers need to be able to handle more simultaneous connections from more application servers.

#### Reverse Proxies

- A reverse proxy can be used if you have multiple application servers or just one. In fact, reverse proxies _should_ be used even if you just have one application server.
- Increase security by hiding information about your application server(s). Can blacklist certain soure IP addresses. Can limit the number of connections from a single client.
- Compresses server responses using technology like `gzip`.
- Handles SSL decryption/encryption, which can be an expensive operation. That frees up application servers to focus only on business logic processing.
- Can be used to cache responses. Cached responses can be served to the client without having to hit the application server again.

#### Warnings

- Load balancers can become performance bottlenecks if they don't have enough resources or are not configured properly.
- Load balancers increase complexity of the system.
- One load balancer is a single point of failure. Configuring multiple load balancers increases complexity of the system.

#### Review Questions

1. What is a load balancer in a nutshell?
2. What are some benefits that a load balancer offers?
3. How can you configure load balancers to handle failures?
    1. How does active-passive fail-over work?
    2. How does active-active fail-over work?
4. Name the different types of algorithms that load balancers use to distribute requests.
    1. What are the pros of each type?
    2. What are the cons of each type?
5. What is a reverse proxy in a nutshell?
6. How is a reverse proxy different than a load balancer?
7. What are some benefits that a reverse proxy offers?
8. What are some things to be careful of when implementing a load balancer?

### Databases

#### Relational Databases

- A collection of data items organized in tables.
- A transaction is a single unit of logic or work, which can be comprised of multiple operations.

> **ACID**
> - **Atomicity:** Each transaction is all or nothing.
> - **Consistency:** Any transaction will bring the database from one valid state to another.
> - **Isolation:** Executing transactions concurrently has the same results as if the transactions were executed serially.
> - **Durability:** Once a transaction has been committed, it will remain so.

**Scaling Relational Databases**

- Master-slave replication
    - Can read and write to master
    - Master replicates writes to slaves
    - Can only read from slaves
    - Slaves can replicate to other slaves in tree-like organization
    - In the event of master failure, system can operate in read-only mode until a slave is promoted to new master or a new master is provisioned
    - Disadvantages:
        - Additional logic needed to promote slave to master
        - Potential loss of data if master fails before newly written data can be replicated to slaves
        - Writes are replayed from master to slaves. Slaves could get bogged down processing writes which will affect their read performance
        - The more slaves, the more replication you have to do, which may slow down the system
        - In some systems, writing to a master can happen quickly if the master can spawn multiple threads to do writes in parallel, but slaves might only processes writes one at a time
        - Replication adds hardware and complexity
- Master-master replication
    - Both masters can read and write and coordinate writes with each other
    - If one master goes down, then the other can continue operating normally
    - Disadvantages:
        - (See master-slave disadvantages)
        - Need a load balancer to distribute connections from the application servers to the databases
        - In lieu of load balancer, need application logic to decide which database to access
        - Loose consistency or increased write latency
        - Conflict resolution comes into play as more write nodes are added and as latency increases
- Federation (Functional Partitioning)
    - Splits up databases by function
        - Example: three databases--forums, users, products
    - Less read/write traffic to each database
    - Less replication lag
    - Can increases throughput by writing to different databases in parallel
    - Smaller databases lead to more data that can be kept in memory, so data can be cached more easily
    - Disadvantages:
        - Not effective if data requires huge functions or tables
        - Added complexity of application code to decide which databases to hit and when
        - Joining data from multiple databases is complicated and uses a server link
        - Adds more hardware and complexity
- Sharding
    - Distributes data across different databases such that a database can only manage a subset of the data
        - Example: users split between shard based on their username--A-C in Shard 1, D-F in Shard 2, G-I in Shard 3, etc.
    - As the amount of data increases, more shards are added
    - Less read/write traffic, less replication, and more cache hits for each shard
    - Index size decreases, which improves performance with faster queries
    - If one shard goes down, then the others will still work
    - Need replication for each shard to prevent data loss
    - Can write in parallel with increases throughput
    - Can split shards by user's last name or by geographic location
    - Disadvantages:
        - Need to program application logic to work with shards--increased complexity
        - Data distribution can become lopsided
        - Rebalancing shards adds complexity
        - Need sharding functions based on consistent hashing
        - Joining data from multiple shards is complex
        - Adds more hardware and complexity
- Denormalization
    - Improves read performance at the expense of write performance
    - Redundant copies of data are written in different tables to avoid JOINs
    - Can use materialized views
    - JOINs are even more expensive in a distributed database system where databases have been federated or sharded
    - On most databases, reads heavily outweigh writes 100:1 or 1000:1
    - Complex JOINs are very expensive and take up most of the time doing disk operations
    - Disadvantages:
        - Data is duplicated
        - Need constraints to help keep redundant copies of data in sync, which adds complexity
        - Denormalized database under heavy write load performs worse than normalized counterpart
- SQL Tuning
    - Benchmark SQL queries to simulate high-load situations
    - Profile SQL queries to help track performance issues
    - Tighten up schema
    - Use good indexes:
        - Faster queries on indexed columns
        - Indexes are represented as self-balancing B-trees, which do operations in logarithmic time efficiency
        - Placing and index can keep the data in memory, requiring more space
        - Writes could be slower because index needs to be updated
        - When loading large amounts of data, it might be faster to disable indexing until the data is loaded, and then rebuild the index
    - Avoid expensive joins by denormalizing data where applicable
    - Partition tables so that "hot spots" are in a separate table to help keep the data in memory
    - Tune the query cache

#### NoSQL Databases

- Collection of data items represented in key-value store, document store, wide column store, or a graph database
- Data is denormalized
- JOINs are generally done in the application code
- Lack true ACID transactions
- Eventual consistency

> **BASE**
> - **Basically available:** the system guarantees availability
> - **Soft state:** the state of the system may change over time, even without input
> - **Eventual consistency:** the system will become consistent over a period of time, given that the system doesn't receive input during that time

**Key-value store**

- Basically a hash table
- Allows for O(1) reads and writes and is backed by memory or SSD
- Maintain keys in lexicographic order, allowing efficient retrieval of key ranges
- Can store metadata with a value
- High-performance
- Good for simple data models or rapidly changing data
- Good for in-memory cache layer
- Limited set of operations
- Application code needs to be written to handle more complex operations

**Document store**

- Basically a hash table with documents as the values
- Centered around XML or JSON documents
- Document stores all data for a given object
- Provides API or query language to search documents based on document structure
- Documents are stored in collections or groups or directories
- Different types of documents can be stored together but may have very different data
- High flexibility
- Good for data that changes occasionally 

**Wide column store**

- Basically a nested map
- Basic unit of data is a column (name/value pair)
- Columns can be grouped in column families (sort of like SQL tables)
- Super column families can further group column families
- Access each column independently with a row key
- Columns with the same row key form a row
- Each value contains a timestamp for versioning and conflict resolution
- Maintain keys in lexicographical order, allowing efficient retrieval of key ranges
- Examples: Cassandra (Facebook), HBase, BigTable (Google)
- Offer high availability and scalability
- Often used for very large data sets

**Graph Database**

- Basically a graph
- Each node is a record
- Each edge is a relationship
- Optimized for complex relationships between nodes

**NoSQL vs. SQL**

- Choose SQL if:
    - Data is structured
    - Strict schema
    - Relational data
    - Need for complex JOINs
    - Transactions
    - Clear patterns for scaling
    - More established; more resources
    - Lookups by index are very fast
- Choose NoSQL if:
    - Semi-structured data
    - Dynamic or flexible schema
    - Non-relational data
    - No need for complex JOINs
    - Store extremely large amounts of data
    - Very data intensive workloads
    - Very high throughput for IOPS
- Sample data well-suited for NoSQL:
    - Rapid ingest of clickstream and log data
    - Leaderboard or scoring data
    - Temporary data such as a shopping cart
    - Frequently accessed "hot" tables
    - Metadata/lookup tables

### Cache

**Cache Locations:**

- Clients (browsers)
- CDNs
- Web servers (reverse proxies)
- Databases (table data, indexes)
- Applications (Redis, memcached)

**What to Cache:**

- Database queries:
    - Hash the query as a key and store query result in cache
        - Disadvantages:
            - Hard to delete cached result with complex queries
            - Have to invalidate all related cached queries if one piece of related data changes in the database
- Objects:
    - Pickle object data or convert it to a different data structure (i.e. JSON document) and store in cache
        - Advantages:
            - Async processes can access the cache
        - Disadvantages:
            - Have to invalidate the cache if the data changes
- Cache targets:
    - User sessions
    - Fully-rendered web pages
    - Activity streams
    - User graph data

#### Caching Strategies

**Cache-aside:**

- Steps:
    1. Look for object in the cache. Return it to the user if present.
    2. If it is not present, then retrieve it from the database.
    3. Add the data to the cache.
    4. Return the data to the user.
- Advantages:
    - Reading cached data is fast
    - Lazy loading
    - Only requested data is cached, so the cache doesn't get filled with unrequested data
- Disadvantages:
    - Each cache miss results in 3 trips, which can cause a noticeable delay
    - Data can become stale if the database data changes (use TTLs or write-throughs)
    - If the cache restarts, then latency will increase as cache is warmed

**Write-through:**

- Steps:
    1. Write data to the cache.
    2. Cache synchronously writes data to the database.
    3. Reads always get the most up-to-date information.
- Advantages:
    - Reads are fast
    - Users are more tolerant of latency on writes than reads
    - Data in the cache is not stale
- Disadvantages:
    - Writes are slow since data has to be written to the database
    - Have to warm the cache on refresh
    - Most data written will never be read (use TTL and cache-aside)

**Write-behind:**

- Steps:
    1. Write data to the cache.
    2. Asynchronously write data to the database in a separate task.
    3. Reads always get the most up-to-date information.
- Advantages:
    - (See write-through)
    - Write performance is improved
- Disadvantages:
    - Potential data loss if the cache fails before it can write to the database
    - More complex to configure
    - Database writes from cache can conflict with database writes from an external source

**Refresh-ahead:**

- Steps:
    1. Predict what data needs to be refreshed, retrieve it and cache it before the user requests it. (Refresh the data right before the TTL expires.)
- Advantages:
    - Reduced latency vs. read-through because likelihood of database query is reduced
- Disadvantages:
    - Performance decreases for every bad prediction made

#### Caching Algorithms

- First in first out (FIFO)
- Last in first out (LIFO)
- Least recently used (LRU)
    - Cache must track what was used and when, which can be expensive
    - Cache can be flooded with undesirable data, which can evict more desirable data
- Most recently used (MRU)
- Least frequently used (LFU)
- Random replacement (RR)

#### Disadvantages

1. Need to maintain consistency between cache and database through cache invalidation.
2. Cache invalidation is hard.
3. Need to make application changes and add software like Redis or memcached.

### Asynchronism

**General Information:**

- Takes load off application server
- Used to do expensive processing tasks that can be thread-blocking
- Executes asynchronously (in the background)
- General flow:
  + Application publishes task to a queue
  + When a worker is available, it pulls the task off the queue and executes it
- Examples:
  + Sending email
  + Contacting external service
  + Running computationally expensive business logic
- Software:
  + Redis
  + RabbitMQ
  + Amazon SQS
  + Celery

**Back Pressure:**

- If task queue is unbounded, then queue size can grow larger than memory can hold and application will crash
- Can maintain high throughput by applying back pressure--limiting the queue size and returning HTTP 503 status code for incoming tasks when queue is full
- Client can retry using exponential backoff algorithm
  + 2^c-1, c = number of times a collision has occurred
  + 1st try (2^1-1 = 1 second)
  + 2nd try (2^2-1 = 3 seconds)
  + 3rd try (2^3-1 = 7 seconds)

**Disadvantages:**

- Adding asychronous job queues adds complexity to the system
- Running tasks asynchronously when you don't need to can cause unnecessary delays in processing

### Communication

**HTTP:**

- Method for encoding and trasporting data between client and server
- Request-response protocol: clients issue requests and servers return responses
- Basic HTTP request:
  + Verb (method)
  + Resource (URI)
  + Headers
  + Payload
- Relies on lower-level protocols such as TCP and UDP

**TCP:**

- Connection-oriented protocol over IP network
- Connection established/terminated with handshake
- Uses packets
- Packets are guaranteed to reach destination in original order and without corruption
- Sender can resend packets if doesn't get correct response back from recipient
- Multiple timeouts lead to connection being dropped
- Uses flow control (prevent sender from overwhelming receiver) and congestion control (prevent sender from overwhelming network)
- Less efficient transmission than UDP
- Web servers can keep large number of TCP connections open, leading to high memory usage
- Useful for applications that require high reliability but are less time critical:
  + Web servers, databases, SMTP, FTP, SSH
- Use when:
  + Need all data to arrive intact
  + Need to make best estimate use of network throughput

**UDP:**

- Connectionless
- Uses datagrams
- Datagrams are only guaranteed at the datagram level (no promises about order)
- Datagrams might reach destination out of order or not at all
- No congestion control
- Generally more efficient than TCP because no guarantees
- Can broadcast datagrams to all devices on subnet
- Less reliable but works well in real-time use cases:
  + VoIP, video chat, streaming, real-time multiplayer games
- Use when:
  + Need lowest latency
  + Late data is worse than loss of data

**RPC:**

- Client causes procedure to execute on different address space, usually remote server
- Procedure is coded as if it were a local procedure call
- Remote calls are slower and less reliable than local calls
- Popular frameworks:
  + Protobuf, Thrift, Avro
- RPC is request-response protocol
- Focused on exposing behaviors
- Used for performance reasons with internal communications (HTTP REST for public external APIs)
- Disadvantages:
  + RPC clients can become tightly coupled to service implementation
  + New API must be defined for every new operation or use case
  + Difficult to debug
  + Might not be able to leverage existing tech out of the box

**REST:**

- Architectural style enforcing client/server model where client acts on a set of resources managed by server
- Server returns representation of resources
- Actions can either get or maninpulate resources
- All communication is stateless and cacheable
- Focused on exposing data
- Minimizes coupling between client and server
- Used for public APIs
- Good for horizontal scaling and partitioning
- Disadvantages:
  + Not great for resources that aren't organized in simple hierarchy
  + Performing actions on the server cannot be captured very well with basic verbs
  + Complex resources are hard to get in a single request--need to make composites or make many calls and join data on client
  + Representations need to change and consumers of the API need to support changes

## Behavioral

### Preparation Grid

| |Project 1|Project 2|Project 3|Project 4|
|-|---------|---------|---------|---------|
|Most Challenging|A|B|C|D|
|What You Learned|A|B|C|D|
|Most Interesting|A|B|C|D|
|Hardest Bug|A|B|C|D|
|Enjoyed Most|A|B|C|D|
|Conflicts with Teammates|A|B|C|D|

> Structure anwers using _S.A.R._ (Situation, Action, Result)

### Most Challenging

**Course Search**

_Situation:_ Students were complaining that our application's course search feature was slow and it wasn't finding the courses they were searching. They were searching for courses by different attributes--course codes (discipline and number), titles, and descriptions.

_Action:_ We added full text and fuzzy search to match search queries to titles and descriptions. We added special code to tokenize attributes differently depending on their types. We implemented ranking and sorting to return results according to relevancy.

_Result:_ The course search performance and accuracy increased. Students reported being happy with the results they were getting during controlled studies.

**Advisor/Student Collaboration: Making Suggestions**

_Situation:_

_Action:_

_Result:_

**Advisor/Student Collaboration: Calculating Differences**

_Situation:_

_Action:_

_Result:_

**Advisor/Student Collaboration: Resolving Transactions**

_Situation:_

_Action:_

_Result:_

**Advisor/Student Collaboration: Locks and Flags**

_Situation:_

_Action:_

_Result:_

**Auto-Loaded Academic Plan**

_Situation:_

_Action:_

_Result:_

**Academic Planning: Planned Courses and Course Groups**

_Situation:_

_Action:_

_Result:_

**Academic Planning: Planning the Same Course in the Same Term**

_Situation:_

_Action:_

_Result:_
